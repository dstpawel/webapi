export class PaymentDetail {
    PMId: number;
    CardOwnerName: string;
    CardNumber: string;
    ExpiriationDate: string;
    CVV: string;
}
